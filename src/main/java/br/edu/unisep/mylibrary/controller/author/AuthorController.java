package br.edu.unisep.mylibrary.controller.author;

import br.edu.unisep.mylibrary.domain.dto.author.AuthorDto;
import br.edu.unisep.mylibrary.domain.usecase.author.FindAllAuthorsUseCase;
import br.edu.unisep.mylibrary.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/author")
public class AuthorController {

    private final FindAllAuthorsUseCase findAllAuthors;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<AuthorDto>>> findAll() {
        var authors = findAllAuthors.execute();

        return authors.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(authors));
    }

}
