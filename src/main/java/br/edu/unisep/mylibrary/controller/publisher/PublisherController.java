package br.edu.unisep.mylibrary.controller.publisher;

import br.edu.unisep.mylibrary.domain.dto.publisher.PublisherDto;
import br.edu.unisep.mylibrary.domain.usecase.publisher.FindAllPublishersUseCase;
import br.edu.unisep.mylibrary.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/publisher")
public class PublisherController {

    private final FindAllPublishersUseCase findAllPublishers;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<PublisherDto>>> findAll() {
        var publishers = findAllPublishers.execute();

        return publishers.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(publishers));
    }

}
