package br.edu.unisep.mylibrary.data.entity.book;

import br.edu.unisep.mylibrary.data.entity.author.Author;
import br.edu.unisep.mylibrary.data.entity.publisher.Publisher;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "edition")
    private Integer edition;

    @Column(name = "publication_year")
    private Integer publicationYear;

    @Column(name = "summary")
    private String summary;

    @Column(name = "pages")
    private Integer pages;

    @Column(name = "isbn")
    private String isbn;

    @OneToOne
    @JoinColumn(name = "author_id", referencedColumnName = "author_id")
    private Author author;

    @OneToOne
    @JoinColumn(name = "publisher_id", referencedColumnName = "publisher_id")
    private Publisher publisher;

}
