package br.edu.unisep.mylibrary.data.repository;

import br.edu.unisep.mylibrary.data.entity.author.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {
}
