package br.edu.unisep.mylibrary.data.repository;

import br.edu.unisep.mylibrary.data.entity.publisher.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Integer> {
}
