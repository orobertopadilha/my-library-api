package br.edu.unisep.mylibrary.domain.builder.author;

import br.edu.unisep.mylibrary.data.entity.author.Author;
import br.edu.unisep.mylibrary.domain.dto.author.AuthorDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthorBuilder {

    public List<AuthorDto> from(List<Author> authors) {
        return authors.stream().map(
                author -> new AuthorDto(author.getId(), author.getName())
        ).collect(Collectors.toList());
    }

}
