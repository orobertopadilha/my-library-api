package br.edu.unisep.mylibrary.domain.builder.publisher;

import br.edu.unisep.mylibrary.data.entity.publisher.Publisher;
import br.edu.unisep.mylibrary.domain.dto.publisher.PublisherDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PublisherBuilder {

    public List<PublisherDto> from(List<Publisher> publishers) {
        return publishers.stream().map(
                author -> new PublisherDto(author.getId(), author.getName())
        ).collect(Collectors.toList());
    }

}
