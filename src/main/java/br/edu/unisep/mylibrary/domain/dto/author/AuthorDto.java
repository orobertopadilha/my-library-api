package br.edu.unisep.mylibrary.domain.dto.author;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AuthorDto {

    private final Integer id;

    private final String name;

}
