package br.edu.unisep.mylibrary.domain.dto.book;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookDto {

    private Integer id;

    private String title;

    private String author;

    private Integer edition;

    private Integer publicationYear;

    private String summary;

    private String publisher;

    private Integer pages;

    private String isbn;

}
