package br.edu.unisep.mylibrary.domain.dto.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterBookDto {

    private String title;

    private Integer authorId;

    private Integer edition;

    private Integer publicationYear;

    private String summary;

    private Integer publisherId;

    private Integer pages;

    private String isbn;

}
