package br.edu.unisep.mylibrary.domain.dto.publisher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PublisherDto {

    private final Integer id;

    private final String name;

}