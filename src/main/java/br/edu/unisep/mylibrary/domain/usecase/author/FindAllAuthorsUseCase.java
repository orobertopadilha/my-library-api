package br.edu.unisep.mylibrary.domain.usecase.author;

import br.edu.unisep.mylibrary.data.repository.AuthorRepository;
import br.edu.unisep.mylibrary.domain.builder.author.AuthorBuilder;
import br.edu.unisep.mylibrary.domain.dto.author.AuthorDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllAuthorsUseCase {

    private final AuthorRepository authorRepository;
    private final AuthorBuilder authorBuilder;

    public List<AuthorDto> execute() {
        var authors = authorRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
        return authorBuilder.from(authors);
    }

}
