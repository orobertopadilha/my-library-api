package br.edu.unisep.mylibrary.domain.usecase.book;

import br.edu.unisep.mylibrary.data.repository.BookRepository;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.BookDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllBooksUseCase {

    private final BookRepository bookRepository;
    private final BookBuilder builder;

    public List<BookDto> execute() {
        var books = bookRepository.findAll();
        return builder.from(books);
    }
}
