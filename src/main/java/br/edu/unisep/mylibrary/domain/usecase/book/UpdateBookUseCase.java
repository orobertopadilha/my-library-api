package br.edu.unisep.mylibrary.domain.usecase.book;

import br.edu.unisep.mylibrary.data.repository.BookRepository;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.UpdateBookDto;
import br.edu.unisep.mylibrary.domain.validator.book.UpdateBookValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateBookUseCase {

    private final UpdateBookValidator validator;
    private final BookBuilder builder;
    private final BookRepository bookRepository;

    public void execute(UpdateBookDto updateBook) {
        validator.validate(updateBook);

        var book = builder.from(updateBook);
        bookRepository.save(book);
    }

}
