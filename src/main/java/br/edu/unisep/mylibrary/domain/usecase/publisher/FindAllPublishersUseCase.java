package br.edu.unisep.mylibrary.domain.usecase.publisher;

import br.edu.unisep.mylibrary.data.repository.PublisherRepository;
import br.edu.unisep.mylibrary.domain.builder.publisher.PublisherBuilder;
import br.edu.unisep.mylibrary.domain.dto.publisher.PublisherDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllPublishersUseCase {

    private final PublisherRepository publisherRepository;
    private final PublisherBuilder publisherBuilder;

    public List<PublisherDto> execute() {
        var publishers = publisherRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
        return publisherBuilder.from(publishers);
    }

}
