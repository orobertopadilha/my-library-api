package br.edu.unisep.mylibrary.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidationMessages {

    public static final String MESSAGE_REQUIRED_BOOK_ID = "Informe o id do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_TITLE = "Informe o título do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_SUMMARY = "Informe o resumo do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_AUTHOR = "Informe o autor do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_PUBLISHER = "Informe a editora do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_ISBN = "Informe o ISBN do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_PAGES = "Informe o número de páginas do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_EDITION = "Informe a edição do livro!";
    public static final String MESSAGE_REQUIRED_BOOK_YEAR = "Informe o ano de publicação do livro!";

}
