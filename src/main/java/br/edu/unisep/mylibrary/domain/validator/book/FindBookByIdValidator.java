package br.edu.unisep.mylibrary.domain.validator.book;


import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.mylibrary.domain.validator.ValidationMessages.MESSAGE_REQUIRED_BOOK_ID;


@Component
public class FindBookByIdValidator {

    public void validate(Integer id) {
        Validate.isTrue(id > 0, MESSAGE_REQUIRED_BOOK_ID);
    }

}
