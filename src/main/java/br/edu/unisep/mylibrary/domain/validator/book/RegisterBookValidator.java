package br.edu.unisep.mylibrary.domain.validator.book;

import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.mylibrary.domain.validator.ValidationMessages.*;

@Component
public class RegisterBookValidator {

    public void validate(RegisterBookDto registerBook) {
        Validate.notBlank(registerBook.getTitle(), MESSAGE_REQUIRED_BOOK_TITLE);
        Validate.notBlank(registerBook.getSummary(), MESSAGE_REQUIRED_BOOK_SUMMARY);
        Validate.notNull(registerBook.getAuthorId(), MESSAGE_REQUIRED_BOOK_AUTHOR);
        Validate.notNull(registerBook.getPublisherId(), MESSAGE_REQUIRED_BOOK_PUBLISHER);
        Validate.notBlank(registerBook.getIsbn(), MESSAGE_REQUIRED_BOOK_ISBN);

        Validate.notNull(registerBook.getPages(), MESSAGE_REQUIRED_BOOK_PAGES);
        Validate.isTrue(registerBook.getPages() > 0, MESSAGE_REQUIRED_BOOK_PAGES);

        Validate.notNull(registerBook.getEdition(), MESSAGE_REQUIRED_BOOK_EDITION);
        Validate.isTrue(registerBook.getEdition() > 0, MESSAGE_REQUIRED_BOOK_EDITION);

        Validate.notNull(registerBook.getPublicationYear(), MESSAGE_REQUIRED_BOOK_YEAR);
        Validate.isTrue(registerBook.getPublicationYear() > 0, MESSAGE_REQUIRED_BOOK_YEAR);
    }

}
