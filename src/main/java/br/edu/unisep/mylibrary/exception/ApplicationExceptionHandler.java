package br.edu.unisep.mylibrary.exception;

import br.edu.unisep.mylibrary.response.DefaultResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class})
    public ResponseEntity<DefaultResponse<Boolean>> handleValidationErrors(Exception exception) {
        return ResponseEntity.badRequest().body(DefaultResponse.of(exception.getMessage(), false));
    }

}
