package br.edu.unisep.mylibrary.domain.builder.book;

import br.edu.unisep.mylibrary.data.entity.author.Author;
import br.edu.unisep.mylibrary.data.entity.book.Book;
import br.edu.unisep.mylibrary.data.entity.publisher.Publisher;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import br.edu.unisep.mylibrary.domain.dto.book.UpdateBookDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class BookBuilderTest {

    private BookBuilder bookBuilder;

    @BeforeEach
    void setup() {
        bookBuilder = new BookBuilder();
    }

    @Test
    void shouldCreateBookDto() {
        var book = new Book();
        book.setId(1);
        book.setTitle("Titulo de teste");
        book.setPages(100);

        var author = new Author();
        author.setId(1);
        author.setName("Author 1");

        var publisher = new Publisher();
        publisher.setId(2);
        publisher.setName("Publisher 2");

        book.setAuthor(author);
        book.setPublisher(publisher);
        book.setSummary("Summary de teste");
        book.setIsbn("1234567890123");
        book.setEdition(2);
        book.setPublicationYear(2021);

        var result = bookBuilder.from(book);

        assertEquals(book.getId(), result.getId());
        assertEquals(book.getTitle(), result.getTitle());
        assertEquals(book.getPages(), result.getPages());
        assertEquals(book.getAuthor().getName(), result.getAuthor());
        assertEquals(book.getPublicationYear(), result.getPublicationYear());
        assertEquals(book.getPublisher().getName(), result.getPublisher());
        assertEquals(book.getSummary(), result.getSummary());
        assertEquals(book.getIsbn(), result.getIsbn());
        assertEquals(book.getEdition(), result.getEdition());
    }

    @Test
    void shouldCreateBookFromRegisterBookDto() {
        var registerBook = new RegisterBookDto();
        registerBook.setTitle("Titulo de teste");
        registerBook.setPages(100);
        registerBook.setAuthorId(101);
        registerBook.setPublisherId(202);
        registerBook.setSummary("Summary de teste");
        registerBook.setIsbn("1234567890123");
        registerBook.setEdition(2);
        registerBook.setPublicationYear(2021);

        var result = bookBuilder.from(registerBook);

        assertNull(result.getId());
        assertEquals(registerBook.getTitle(), result.getTitle());
        assertEquals(registerBook.getPages(), result.getPages());
        assertEquals(registerBook.getAuthorId(), result.getAuthor().getId());
        assertEquals(registerBook.getPublicationYear(), result.getPublicationYear());
        assertEquals(registerBook.getPublisherId(), result.getPublisher().getId());
        assertEquals(registerBook.getSummary(), result.getSummary());
        assertEquals(registerBook.getIsbn(), result.getIsbn());
        assertEquals(registerBook.getEdition(), result.getEdition());
    }

    @Test
    void shouldCreateBookFromUpdateBookDto() {
        var updateBook = new UpdateBookDto();
        updateBook.setId(1);
        updateBook.setTitle("Titulo de teste");
        updateBook.setPages(100);
        updateBook.setAuthorId(101);
        updateBook.setPublisherId(202);
        updateBook.setSummary("Summary de teste");
        updateBook.setIsbn("1234567890123");
        updateBook.setEdition(2);
        updateBook.setPublicationYear(2021);

        var result = bookBuilder.from(updateBook);

        assertEquals(updateBook.getId(), result.getId());
        assertEquals(updateBook.getTitle(), result.getTitle());
        assertEquals(updateBook.getPages(), result.getPages());
        assertEquals(updateBook.getAuthorId(), result.getAuthor().getId());
        assertEquals(updateBook.getPublicationYear(), result.getPublicationYear());
        assertEquals(updateBook.getPublisherId(), result.getPublisher().getId());
        assertEquals(updateBook.getSummary(), result.getSummary());
        assertEquals(updateBook.getIsbn(), result.getIsbn());
        assertEquals(updateBook.getEdition(), result.getEdition());
    }

    @Test
    void shouldCreateListOfBookDto() {
        var books = Arrays.asList(mock(Book.class), mock(Book.class), mock(Book.class));

        var result = bookBuilder.from(books);

        assertNotNull(result);
        assertEquals(3, result.size());
    }

}
