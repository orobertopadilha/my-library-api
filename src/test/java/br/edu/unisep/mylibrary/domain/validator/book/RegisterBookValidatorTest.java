package br.edu.unisep.mylibrary.domain.validator.book;

import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static br.edu.unisep.mylibrary.domain.validator.ValidationMessages.MESSAGE_REQUIRED_BOOK_AUTHOR;
import static br.edu.unisep.mylibrary.domain.validator.ValidationMessages.MESSAGE_REQUIRED_BOOK_TITLE;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RegisterBookValidatorTest {

    private RegisterBookValidator registerBookValidator;

    @BeforeEach
    void setup() {
        this.registerBookValidator = new RegisterBookValidator();
    }

    @Test
    void shouldValidateTitleNullRequired() {
        var registerBook = new RegisterBookDto(null,
                1, 1, 2021,
                "Resumo de teste", 2,
                1000, "1234567890123");

        assertThrows(NullPointerException.class, () -> registerBookValidator.validate(registerBook), MESSAGE_REQUIRED_BOOK_TITLE);
    }

    @Test
    void shouldValidateTitleEmptyRequired() {
        var registerBook = new RegisterBookDto("",
                1, 1, 2021,
                "Resumo de teste", 2,
                1000, "1234567890123");

        assertThrows(IllegalArgumentException.class, () -> registerBookValidator.validate(registerBook), MESSAGE_REQUIRED_BOOK_TITLE);
    }

    @Test
    void shouldValidateAuthorNullRequired() {
        var registerBook = new RegisterBookDto("Titulo de Teste",
                null, 1, 2021,
                "Resumo de teste", 2,
                1000, "1234567890123");

        assertThrows(NullPointerException.class, () -> registerBookValidator.validate(registerBook), MESSAGE_REQUIRED_BOOK_AUTHOR);
    }


}
